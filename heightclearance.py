""" script to check if car fits through garage gate """

import math
import matplotlib.pyplot as plt

# measurements:
carheight = 198.00
carlength = 500.00
garageheight = 201.20
wheeldist = 310.00
heightdiff = 13.00             # difference in height between floor at gate and heightdiff_dist
heightdiff_dist = 400.00       # distance between gate and outer height measuring point

frontwheelstartxpos = -carlength + carlength / 2.0 - wheeldist / 2.0 
rearwheelstartxpos = -carlength + carlength / 2.0 + wheeldist / 2.0 
slant = heightdiff/heightdiff_dist
slant_degrees = math.degrees(math.atan(heightdiff/heightdiff_dist))

datax = []
datay = []
slope = []

for i in range(0,int(wheeldist)):
    # calculate front and back wheel positions:
    if  -frontwheelstartxpos - i >= 0:
        frontwheelxpos = frontwheelstartxpos + i
        frontwheelypos = 0
    else:
        frontwheelxpos = math.cos(math.radians(slant_degrees)) * (i + frontwheelstartxpos) 
        frontwheelypos = slant * frontwheelxpos
    if -rearwheelstartxpos - i >= 0:
        rearwheelxpos = rearwheelstartxpos + i
        rearwheelypos = 0
    else:
        rearwheelxpos = math.cos(math.radians(slant_degrees)) * (i + rearwheelstartxpos) 
        rearwheelypos = slant * rearwheelxpos
    #distcheck = math.sqrt((rearwheelxpos-frontwheelxpos)**2+(rearwheelypos-frontwheelypos)**2)
    #print(distcheck)
	# rotate around lower left anchor:
    theta = math.asin(rearwheelypos/wheeldist)
    x1 = frontwheelxpos
    y1 = frontwheelypos
    x2 = frontwheelxpos
    y2 = frontwheelypos + carheight
    x3 = math.cos(theta) * (x2-x1) - math.sin(theta) * (y2-y1) + x1
    y3 = math.sin(theta) * (x2-x1) + math.cos(theta) * (y2-y1) + y1
    print(y2,y3)
    heightatgate = abs(x3) * math.tan(theta) + y3
    datax.append(frontwheelxpos)
    datay.append(heightatgate)    
  
plt.plot(-0.5 * wheeldist, garageheight, marker='o', markersize=3, color="red")
plt.annotate('gate: ' + str(round(garageheight,2)), (-0.5 * wheeldist,garageheight), textcoords="offset points", xytext=(-60,-10))
plt.plot(datax,datay)
for x,y in zip(datax,datay):
    if x == -0.5 * wheeldist:
        plt.plot(x,y, marker='o', markersize=3, color="red")
        plt.annotate('max: '+str(round(y,2)), (x,y), textcoords="offset points", xytext=(0,5))
plt.gcf().canvas.set_window_title('Garage Gate Top Clearance')
plt.xlabel('front wheel position relative to gate (cm)',fontsize=12)
plt.ylabel('car roof height at gate intersect (cm)',fontsize=12)
plt.show()
    
    
    

