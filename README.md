# Car Height Clearance Calculator

Some code to calculate and plot the height clearance under a garage door that goes out unto a slope. I needed this to check if a certain campervan would get stuck or not.

Parameters can be set in the code:
- carheight
- carlength
- garageheight (lowest spot)
- wheeldist (script assumes midpoint between wheels is at car center)
- heightdiff (height offset at heightdiff_dist from lowest spot)
- heightdiff_dist (e.g. in my case we measured 13cm difference at 400cm from gate)